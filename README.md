# Container_Security

My notes about the [book](https://www.lizrice.com/projects) Container Security  

1. [Chapter 1 : Container Security Threats](#chapter1) 
2. [Chapter 2 : Linux System Calls, Permissions and Capabilities](#chapter2)
3. [Chapter 3 : Control Groups](#chapter3)
4. [Chapter 4 : Container Isolation](#chapter4)
5. [Chapter 5 : Virtual Machines](#chapter5)
6. [Chapter 6 : Container Images](#chapter6)
7. [Chapter 7 : Software Vulnerabilities in Images](#chapter7)

<a name="chapter1"></a>
# Chapter 1 : Container Security Threats
 
 <a name="chapter2"></a>
# Chapter 2 : Linux System Calls, Permissions and Capabilities
	- System Calls : requests from user space (ring 3) to kernel (~300) (ring 0)
	- SetUID & SetGID :  Run program as the original owner (run ping as root even if you are a user) *OLD*
	- SetUID does not work on bash, but can work on a custom app which will launch bash (or any shell)
	- Sticky Bit on Directory : Users can create files and Write in them, but only the owner can delete files
	- Sticky Bit on Files : That file stay open in memory, not handled by modern Linux OS
	- Linux Capabilities : Fine tuning, more granular way of managing rights/capabilities (CAP_NET_RAW capa for ping) *NEW*
	- getcaps
	
<a name="chapter3"></a>
# Chapter 3 : Control Groups
	- Cgroups limit the resources, such as memory, CPU, and network I/O a process can use
	- Each hierarchy is managed by a cgroup controller
	- Creating Cgroups : create a subdir inside the memory dir, creates a cgroup for (say) an app
	- lscgroup
	- Can set resources limit (ex : memory.limit_in_bytes)
	- Assign a Process to a Cgroup : write PID into the cgroup.procs file for the cgroup
	- Docker uses cgroups
	- Cgroups v2 : a process can't join different groups for different controllers
	
<a name="chapter4"></a>
# Chapter 4 : Container Isolation
## Linux Namespaces
    Control what a process can “see”
    7/8 namespaces : Unix TimeSharing System, UTS (hostname and domain name), PIDs, Mount points, Network, User and group IDs, IPC, cgroups, (time ?)
    A process is in exactly one namespace of each type
    lsns (as root to get the full picture)
## Isolating the Hostname (UTS)
    sudo unshare --uts sh (can define new hostname/domain name. ex : docker container hostnames)
## Isolating Process IDs
    sudo unshare --pid --fork sh (ps will return too much info, cause reading virtual files in /proc)
    → need a new /proc directory for the kernel to write information about the processs inside the new namespace in order for the process
    to see only his namespace PIDs
## Changing the Root Directory
    chroot runs COMMAND in the NEWROOT directory [...] it runs ${SHELL} -i by default (/bin/sh -i)
    pivot_root : the old root is no longer mounted. It takes advantage of the mount namespace.
## Combine Namespacing and Changing the Root
    sudo unshare --pid --fork chroot alpine sh
    Having its own “/proc” allows the process to write its own virtual files in it, therefor seeing only its own processes
## Mount Namespace
    Allows to bind some hosts directories into the container. In order for the container to only see 
    its own mounted bind, in the same way as PIDs namespace, it needs to have its own root filesystem with a /proc directory (findmnt [target])
## Network Namespace 
    Allows container to have its own view of network interfaces and routing tables. (lsns)
    sudo unshare --net bash : starts with just the loopback interface
    ip link add ve1 netns 28586[this bash pid] type veth peer name ve2 netns 1
    interfaces must be brought up
    routes must be added on the host as well as in the container
## User Namespace
    Allows processes to have their own view of user and group IDs
    A user can have different ID on host and container (ex : 1000 on host, 0 aka root inside the container)
    Creating a process with several namespaces, User will be the first to be created as it grants the requiered 
      capabilities that permits to create other namespaces
    Allows for rootless containers
    When interacting inside a container with a file mounted from the host, it's the effective UID that applies (the one on the host)
    Does not work with K8S (at this time)
## Inter-Process Communications Namespace
    A shared range of memory for process to communicate together
    Once in each container
## Cgroup Namespace
    Isolate the host resources attributed to a process (CPU, Memory, Network I/O)
## Container Processes from the Host Perspective
    A process running inside a container will always have a (much) 
    higher PID on the host than inside the container
## Container Host Machines
    as little user as possible
    best use “Thin OS” : less components = less vulnerabilities
    Same conf / immutability : easier to spot unwanted changes

<a name="chapter5"></a>		
# Chapter 5 : Virtual Machines
## Booting up a machine :  Bios (Basic Input Output System) -> UEFI (Unified Extensible Firmware Interface)
    1 : Enumeration : checks available memory, network interfaces, devices, storage, ...
    2 : Bootloader loads and run OS kernel code -> High level of privilege -> interact with memory, network interfaces, ...
    3 : Kernel runs at ring 0 and user space code runs at ring 3
    4 : Kernel code runs on the CPU as machine code instructions, some are privileged instructions to access memory, start cpu threads, etc
    5 : boot : mount root FS, set up networking, bring up any system deamon
    6 : Once kernel has finished its own initialization, it starts running programs in user space and manage everything 
        the prog needs (starts manage and schedules CPU threads, assigns blocks of memory to each
        process and makes sure that processes can't access one another's memory blocks.
## Enter the Virtual Machine Monitor (VMM)
    it assigns ressources (CPU, MEM, NET, DEV) and starts a guest kernel
    Gives each guest kernel only the details of the subset that it is being given access to. 
    VMM is responsible guest OS and guest apps can't breach the boundaries of their allocated ressources.
### Type 1 VMMs, Hypervisors
    Dedicated kernel-level VMM program runs directly on top of the hardware (at ring 0) instead of a regular OS kernel.
    Guest OS kernel then runs at ring 1.
### Type 2 VMMs, "hosted"
    VirtualBox-like, the VMM runs in the user space, on top on the host OS, and the guest OS on top of the VMM.
    The VMM needs to install privileged components to provide virtualization.
### Kernel-Based Virtual Machines (KVM)
    Between type 1 and type 2.
    A VMM that runs within the host OS kernel.
    KVM type 1, QEMU type 2.
    QEMU can take advantage of hardware acceleration offered by KVM.
## Trap-and-Emulate
    CPU privileged instructions run at a higher ring than ring 0 will cause a trap.
    The CPU will call for a handler in the ring 0 code.
    If the VMM runs at ring 0, a privileged instruction run by the guest can invoke a handler in the VMM. (isolation between guest OSs)
    The set of CPU instructions that can affect the machine's ressourcesis known as "sensitive".
    Instructions that are "sensitive" but not privileged are considered to be "non-virtualizable".
## Handling Non-Virtualizable Instructions
    * Binary Translation -> NPS instructions are rewritten by the VMM in real time.
    * Paravirtualization -> Guest OS is rewritten to avoid usgin NPS set of instructions (XEN). SYSCALL to hypervisor.
    * Hardware virtualization -> hypervisor run in VMX root mode (ring -1) so VM guest OS kernels run on VMX non-root mode (Ring 0) (Intel's VT-x).
## Process Isolation and Security
    Kernel manages its user space processes, including memory.
    Speculative Processing -> better perfs but Spectre and Metldown
    Hypervisor have a simpler job
    In a kernel processes are allowed some visibility of each other (ps command, /proc dir, IPC, shared memory, ...) -> weaker isolation
    You can't see one VM processes from another, less code ine the hypervisor. Smaller and simpler than full kernels. 
        -> less attack surface. Strong isolation boundaries.
## Disavantages of Virtual Machines
    Much slower start up time than containers, hence sluggish for auto-scaling. (Amazon Firecracker has 100ms start up time though)
    Containers allow to "build once, run everywher"
    In a cloud provider, you have to pay for the VM CPU and MEM, regardless of how you use it
    VM have overhead of running the whole kernel. Sharing one (container) is more efficient in ressources and perfs.
## Container Isolation Compared to VM Isolation
    Containers are Linux processes with restricted view.
    Sharing a kernel makes isolation weaker.
	    
<a name="chapter6"></a>
# Chapter 6 : Container Images 
## Root Filesystem and Image Configuration
    Dockerfile commands like FROM, ADD, COPY, RUN modify the content of the root fs.
    Commands like USER, PORT, ENV affects the configuration information stored in the image alongside the root fs.
    Docker inspect toe see config information
## Overriding Config at Runtime
    with docker run -e <VARNAME>=<NEWVALUE>
    with k8s in the yaml file :
        env:
        - name: VARNAME
          value: "this overrides the value"
## OCI Standards
    Open Container Initiative. A lot in common with docker.
    Skopeo to manipulate OCI images and convert docker images into OCI-format.
    runc is OCI-compliant. It needs the image to be unpacked :
        a root fs
        a config.json file that defines runtime settings
    use docker image inspect to see these informations
## Image Configuration
    config.json files defines cgroups, namespaces, ...
## Building Images
### The dangers of docker build
    The "docker" command converts your command into an API request that it sends to the Docker daemon via the Docker socket.
    The Docker daemon has to run as root to create namespaces.
    Any user that has access to the docker API can trigger a build and perform a docker run on the machine.
    Hard to track (the audit will record the daemon process ID instead of the user).
### Daemonless Builds
    BuildKit (Moby project) can also run in rootless mode
    Podman (Red Hat)
    Bazel (google). Generates images deterministically (same image from same source).
    Kaniko (google). For running builds within a k8s cluster.
    img (Jess Frazelle), orca-build (Aleksa Sarai).
### Image Layers
#### Sensitive data in layers
    Don't store sensitive data in a image. Any file that ever existed in any layer can be obtained by unpacking the image.
    docker save imagename > imagename.tar lets you explore each layer of the image (referenced in the .json file)
## Storing Images
    Docker Hub, Amazon's Elastic Container Registry, Google Container Registry.
    Each layer is stored (only once) as a "blob" of data in the registry, identified by a hash of its content.
    The manifest identifies the set of image layer blobs that make up the image. hash that manifest and get the image digest.
## Identifying Images
    By hash : more reliable but not very human friendly.
    By tag : easy to use, but can be reassigned at will.
    In case of scanning an image for vulnerability refering to the image by hash makes more sense as it will be scanned.
    at evey change of the hash while the tag can stay the same.
## Image Security
    SAST and DAST tools, peer review, and testing can help identify insecurities.
## Build-Time Security

### Provenance of the Dockerfile
    Risks include :
    * adding malware into the image
    * accessing build secrets
    * enumerating the network topology accessible from the build infrastructure
    * attacking the build host
### Dockerfile best Practices for Security
    1 -> Base image :
    * trusted registry
    * preapproved or "golden" base image
    * the smaller the image, the smaller the attack surface (distroless)
    * choose between tag and digest wisely
    2 -> Use multi-stage builds :
    Eliminate unecessary content in the final image.
    ex : one stage does compilation and creates a binary executable
         second stage just access the standalone executable
    The deployed image has a smaller attack surface
    3 -> Dockerfile configuration best practices
    * non-root USER
    * RUN commands
        check or audit log on new or modified RUN commands in the Dockerfile
    * Volume mounts
        check that directories like /etc or /bin are not mounted into a container
    * No sensitive data in the Dockerfile
    * Avoid setuid binaries
    * Avoid unneccessary code
    * Include everything that your container needs
### Attacks on the Build Machine
    -> can an attacker reach other parts of the system if he breaches the build machine ?
    -> can an attacker influence the outcome of a build ?
    Run builds on a separate machine or cluster of machines from the production env.
## Image Storage Security
### Running Your Own Registry
### Signing Images
    Image signing based on the Notary implementation of the TUF (The Update framework)
    In-toto carrying security-related metadata from each step through the process
## Image Deployment Security
    Admission control
### Deploying the Right Image
    Using image tag or image digest
    k8s imagePullPolicy (unnecessary if you refer to the image by it's digest)
    Ensure provenance by checking for an image signature (Notary).
### Malicious Deployment Definition
    Check origin of yaml files in k8s.
    Don't use internet yaml without control.
### Admission Control
    About to deploy a resource into a cluster :
        -> has the image been scanned for vulns/malware/... ?
        -> does the image comes from a trusted registry ?
        -> is the image signed ?
        -> is the image approved ?
        -> does the image run as root ?
## GitOps and Deployment Security
    Infrastructure As Code
    Everything is done under source code control system
    Audit trail for every operation
    
<a name="chapter7"></a>
# Chapter 7 : Software Vulnerabilities in Images
    Responsible Security Disclosure (time to publish vuln)
    CVE : Common Vulnerability and Exposures
    The MITRE oversees a number of CNA(CVE Numbering Authorities) (M$, Oracle, RedHat, GitHub,...)
    CVE IDs are used in the National Vulnerability Database
## Vulnerabilities, Patches, and Distributions
    Cross Reference NVD and security advisories of the distribution
    Distro can be hard to patch du to interdependancies
    Container solve this issue by having their own filesystem
## Application-Level Vulnerabilities
    Applications can be scanned for vulnerabilities in third party packages (installed trough pip, npm, maven...)
    Or shared libraries (Go, C, Rust, ...)
## Vulnerability Risk Management
    Vulnerability scanners to assess severity, prioritize and fix/mitigate the issue
## Vulnerability Scanning
    Scan from inside, what is installed in a root FS.
    * Root FS can have vuln
    * Package manager can install packages with vuln in it
    * Avoid using direct install (wget/curl) as packages won't be tracked
## Installed Packages
    Don't update packages in the container directly.
    Build a new machine image with updated packages or update automation scripts.
## Container Image Scanning
    Scan the base image used for containers and not containers themselves.
    Containers must be immutable.
### Immutable Containers
    Drift prevention
    Read Only FS (with restricted temp local storage)
### Regular Scanning
    New vulns are uncovered on a regular basis.
    Scanning base image every 24h and at every build is a good practice.
## Scanning Tools
    Open Source : Trivy, Clair, Anchore 
    Commercial : JFrog, Palo Alto, Aqua
    Image registry solutions : Docker Trusted Registry, CNCF project Harbor (and built-in featrures of the major Cloud Provider)
### Sources of Information
    If a scanner rely only on the NVD and does not cross check with distro specific vuln/patch there will be false positives.
### Out-of-Date Sources
    For example Alpine switched from alpine-secdb to aports. Be sure that your scanner have up-to-date sources
### Won't Fix Vulnerabilities
    Negligible risk or nontrivial fix or not to break compat.
### Subpackage Vulnerabilities
    Scanner may assume that if a package is installed, all the subpackages are installed as well. It is not always true and may
    lead to false positives. (ex the bind package + docs)
### Package Name Differences
    A package may include binaries with completely different names (in Debian : shadow includes login, passwd and uidmap).
    May results in false negatives.
### Additional Scanning Features
    * known malware within the image
    * Executables with the setuid bit
    * Images configured to run as root
    * Secret credentials such as tokens or passwords
    * Sensitive data (credit cards, social security, ...)
### Scanner Errors
    That happens ! Whitelist false positives and STFU
## Scanning in the CI/CD Pipeline
    Developer scanning : local scan by the dev        
    Scan on build : If high vulns are found, the build fails
    Registry scans : Scan registry images on a regular basis
## Preven Vulnerable Images from Running
    Install a check so that only scanned images can be deployed (admission control)
    k8s : Open Policy Agent
    google : Kritis
## Zero-Day Vulnerabilities
    Sandboxing (chapter 8)
    Detect and prevent anomalous behavior at runtime (chapter 13)
